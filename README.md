# MovieGrid

App that uses the data from the the Movie Database API and displays the movie poster thumbnails in a grid layout. App load an initial set of movie data for 20 movies, display those in a grid and when the user scrolls down progressively load another set of 20 movies.
API: https://developers.themoviedb.org/3/getting-started/introduction
