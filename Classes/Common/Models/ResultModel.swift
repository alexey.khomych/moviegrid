//
//  ResultModel.swift
//  MovieGrid
//
//  Created by Alexey Khomych on 21.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import Foundation

struct ResultModel: Codable {
    let results: [MovieModel]
    let page: Int
    let totalResults: Int
    let dates: DatesModel
    let totalPages: Int
    
    enum CodingKeys: String, CodingKey {
        case results, page
        case totalResults = "total_results"
        case dates
        case totalPages = "total_pages"
    }
}
