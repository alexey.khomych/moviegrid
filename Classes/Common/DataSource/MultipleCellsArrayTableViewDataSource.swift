//
//  MultipleCellsArrayTableViewDataSource.swift
//  MovieGrid
//
//  Created by Alexey Khomych on 22.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import Foundation
import UIKit

protocol MultipleCellsFactory {
  associatedtype Model
  
  func cellForModel(_ model: Model, tableView: UITableView, indexPath: IndexPath) -> UITableViewCell
}

class MultipleCellsArrayTableViewDataSource<Model: Any, CellsFactory: MultipleCellsFactory>: NSObject, UITableViewDataSource
where CellsFactory.Model == Model {
  
  // MARK: - Injected
  
  var cellsFactory: CellsFactory!
  
  // MARK: - Public Variables
  
  private(set) var items = [Model]()
  
  // MARK: - Public
  
  func updateWith(items: [Model]) {
    self.items = items
  }
  
  func append(items: [Model]) {
    self.items.append(contentsOf: items)
  }
  
  func replace(item: Model, at index: Int) {
    guard items.count > index else {
      return
    }
    
    items[index] = item
  }
  
  func objectAtIndexPath(_ indexPath: IndexPath) -> Model {
    return items[indexPath.row]
  }
  
  // MARK: - UITableViewDataSource
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return items.count > 0 ? 1 : 0
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return items.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let model = items[indexPath.row]
    return cellsFactory.cellForModel(model, tableView: tableView, indexPath: indexPath)
  }
}
