//
//  SectionType.swift
//  MovieGrid
//
//  Created by Alexey Khomych on 22.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import Foundation

protocol SectionType {
  associatedtype Model
  
  var items: [Model] { get set }
}
