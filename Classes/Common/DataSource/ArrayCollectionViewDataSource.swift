//
//  ArrayCollectionViewDataSource.swift
//  MovieGrid
//
//  Created by Alexey Khomych on 21.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import UIKit

class ArrayCollectionViewDataSource<Model: AnyObject, CellType: UICollectionViewCell>: NSObject, UICollectionViewDataSource
    where CellType: ModelTransfer, CellType.ModelType == Model {

    // MARK: - Public Variables

    private(set) var items = [Model]()

    // MARK: - Public

    func updateWith(items: [Model]) {
        self.items = items
    }

    func append(items: [Model]) {
        self.items.append(contentsOf: items)
    }

    func objectAtIndexPath(_ indexPath: IndexPath) -> Model {
        return items[indexPath.row]
    }

    func indexPathForObject(_ object: Model) -> IndexPath? {
        guard let index = items.firstIndex(where: { $0 === object }) else {
            return nil
        }
        return IndexPath(row: index, section: 0)
    }

    func removeObjectAtIndexPath(_ indexPath: IndexPath) -> Model? {
        if items.count > indexPath.row {
            return items.remove(at: indexPath.row)
        }
        return nil
    }

    // MARK: - UICollectionViewDataSource

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return items.count > 0 ? 1 : 0
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let reuseIdentifier = CellType.reuseIdentifier()
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! CellType

        let model = items[indexPath.row]
        cell.update(with: model)

        return cell
    }
}
