//
//  ModelTransfer.swift
//  MovieGrid
//
//  Created by Alexey Khomych on 21.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import Foundation

protocol ModelTransfer {
    /// Type of model that is being transferred
    associatedtype ModelType

    /// Updates view with `model`.
    func update(with model: ModelType)
}
