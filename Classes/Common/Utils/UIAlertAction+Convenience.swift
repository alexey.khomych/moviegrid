//
//  UIAlertAction+Convenience.swift
//  MovieGrid
//
//  Created by Alexey Khomych on 21.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import UIKit

typealias AlertActionBlock = () -> ()

private extension UIAlertAction {
  static func wrapAction(_ action: AlertActionBlock? = nil) -> (UIAlertAction) -> () {
    return { _ in
      action?()
    }
  }
}

// MARK: - Common

extension UIAlertAction {
    static func okAction(_ action: AlertActionBlock? = nil, style: UIAlertAction.Style = .cancel) -> UIAlertAction {
        return UIAlertAction(title: GlobalConstants.okTitle, style: style, handler: wrapAction(action))
    }
  
    static func cancelAction(_ action: AlertActionBlock? = nil) -> UIAlertAction {
        return UIAlertAction(title: GlobalConstants.cancelTitle, style: .cancel, handler: wrapAction(action))
    }
}
