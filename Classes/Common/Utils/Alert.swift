//
//  Alert.swift
//  MovieGrid
//
//  Created by Alexey Khomych on 21.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import UIKit

protocol PresentableAsAlert {
    func alertController() -> UIAlertController
}

extension UIAlertController {
    static func alertController(title: String?, message: String?, actions: [UIAlertAction], style: UIAlertController.Style = .alert) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: style)
        actions.forEach { alert.addAction($0) }
        
        return alert
    }
}

enum Alert {
    case error(message: String)
}

extension Alert: CustomStringConvertible {
    var description: String {
        switch self {
        case let .error(message):
            return message
        }
    }
}

extension Alert: PresentableAsAlert {
    func alertController() -> UIAlertController {
        return .alertController(title: title, message: description, actions: actions)
    }
    
    func actionSheet() -> UIAlertController {
        return .alertController(title: title, message: !description.isEmpty ? description : nil, actions: actions, style: .actionSheet)
    }
}

private extension Alert {
    var title: String? {
        switch self {
        case .error:
            return nil
        }

    }

    var actions: [UIAlertAction] {
        switch self {
        case .error:
            return [ .okAction() ]
        }
    }
}
