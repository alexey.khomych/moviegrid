//
//  EnvironmentType.swift
//  MovieGrid
//
//  Created by Alexey Khomych on 21.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import Foundation

protocol EnvironmentType {
    var baseUrl: String { get }
    var apiKey: String { get }
    var basePosterUrl: String { get }
}

struct Environment: EnvironmentType {
    var baseUrl: String
    var apiKey: String
    var basePosterUrl: String
}
