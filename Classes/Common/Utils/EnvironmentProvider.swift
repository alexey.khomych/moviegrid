//
//  EnvironmentProvider.swift
//  MovieGrid
//
//  Created by Alexey Khomych on 21.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import Foundation

protocol EnvironmentProvider {
    func loadEnviroment() -> EnvironmentType
}

private struct EnvironmentKeys {
    static let baseUrl = "BaseURL"
    static let apiKey = "APIKey"
    static let basePosterUrl = "BasePosterURL"
}

class EnvironmentProviderImpl: EnvironmentProvider {
    
    func loadEnviroment() -> EnvironmentType {
        let bundle = Bundle(for: type(of: self))

        guard let infoDict = bundle.infoDictionary else {
            fatalError("No info plist configuration in bundle!")
        }
        
        guard let baseUrl = infoDict[EnvironmentKeys.baseUrl] as? String else {
            fatalError("Info.plist has no googleMapsAPIKey for needed configuration!")
        }
        
        guard let apiKey = infoDict[EnvironmentKeys.apiKey] as? String else {
            fatalError("Info.plist has no googlePlacesAPIKey for needed configuration!")
        }
        
        guard let basePosterUrl = infoDict[EnvironmentKeys.basePosterUrl] as? String else {
            fatalError("Info.plist has no googlePlacesAPIKey for needed configuration!")
        }

        return Environment(baseUrl: baseUrl, apiKey: apiKey, basePosterUrl: basePosterUrl)
    }
}
