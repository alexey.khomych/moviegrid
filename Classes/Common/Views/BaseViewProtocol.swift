//
//  BaseViewProtocol.swift
//  MovieGrid
//
//  Created by Alexey Khomych on 21.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import UIKit

protocol BaseViewProtocol: class {
    func presentAlert(_ alert: Alert)
}

extension UIViewController: BaseViewProtocol {
    
    func presentAlert(_ alert: Alert) {
        lastestPresentedController.present(alert.alertController(), animated: true, completion: nil)
    }
}

fileprivate extension UIViewController {
    
    private var lastestPresentedController: UIViewController {
        var presentingController = self
        // find last free view controller to present alert,
        // e.g. if other alert is already presented - we need to present on top of it
        while presentingController.presentedViewController != nil {
            // force unwrap is ok here, as we've checked for nil in while loop
            presentingController = presentingController.presentedViewController!
        }
        return presentingController
    }
}
