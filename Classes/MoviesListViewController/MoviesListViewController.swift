//
//  MoviesListViewController.swift
//  MovieGrid
//
//  Created by Alexey Khomych on 21.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import UIKit

class MoviesListViewController: UIViewController {
    
    // MARK: - Outlets
    
    @IBOutlet private weak var collectionView: UICollectionView!
    
    // MARK: - Private Variables
    
    private let movieService = MovieServiceImpl()
    private let dataSource = ArrayCollectionViewDataSource<MoviesCollectionItemViewModel, MoviesListCollectionViewCell>()
    private var dataStorage: [MovieModel] = []
    private var refreshControl = UIRefreshControl()
    
    // MARK: - View lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        loadData()
        configureUI()
    }
    
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        if let layout = collectionView.collectionViewLayout as? MoviesCollectionViewLayout {
            let cellPerRow = UIDevice.current.orientation.isPortrait ? Constants.cellPerRowForPortraitOrientation : Constants.cellPerRowForLandscapeOrientation
            layout.cellPerRow = cellPerRow
        }
    }
}

// MARK: - Private

private extension MoviesListViewController {
    
    enum Constants {
        static let rightImageName = "common-user-icon"
        static let navigationTitle = "Latest Movies"
        static let cellPerRowForPortraitOrientation: CGFloat = 2.0
        static let cellPerRowForLandscapeOrientation: CGFloat = 3.0
    }
    
    @IBAction func onShowMoreTap() {
        loadNext()
    }
    
    func configureUI() {
        setupNavigationBarItems()
        setupNavigationBarAppearence()
        setupRefrechControl()
        
        collectionView.dataSource = dataSource
        collectionView.collectionViewLayout = MoviesCollectionViewLayout()
    }
    
    func setupNavigationBarItems() {
        let rightButton = UIButton(type: .custom)
        rightButton.setImage(UIImage(named: Constants.rightImageName), for: .normal)
        rightButton.imageView?.contentMode = .scaleAspectFit
        rightButton.frame = CGRect(x: 0, y: 0, width: 35, height: 35)
        rightButton.addTarget(self, action: #selector(onProfileTap), for: .touchUpInside)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightButton)
        navigationItem.title = Constants.navigationTitle
    }

    func setupNavigationBarAppearence() {
        let navigationBarAppearace = navigationController?.navigationBar
        navigationBarAppearace?.tintColor = #colorLiteral(red: 0.5176470588, green: 0.5450980392, blue: 0.6392156863, alpha: 1)
        navigationBarAppearace?.barTintColor = .white
        navigationBarAppearace?.titleTextAttributes = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.5176470588, green: 0.5450980392, blue: 0.6392156863, alpha: 1)]
    }
    
    func setupRefrechControl() {
        refreshControl.addTarget(self, action: #selector(loadData), for: .valueChanged)
        collectionView.addSubview(refreshControl)
    }
    
    func loadNext() {
        movieService.loadNext { result in
            switch result {
            case let .success(items):
                self.prepareModelsForDataSource(items)
            case let .failure(error):
                self.presentAlert(.error(message: error.errorDescription))
            }
        }
    }
    
    func prepareModelsForDataSource(_ models: [MovieModel]) {
        dataStorage += models
        let viewModels = models.compactMap { MoviesCollectionItemViewModel(posterURL: $0.posterPath ?? "") }
        dataSource.append(items: viewModels)
        collectionView.reloadData()
    }
    
    @objc func onProfileTap() {
        
    }
    
    @objc func loadData() {
        movieService.nowPlaying { result in
            switch result {
            case let .success(items):
                self.prepareModelsForDataSource(items)
            case let .failure(error):
                self.presentAlert(.error(message: error.errorDescription))
            }
            
            if self.refreshControl.isRefreshing {
                self.refreshControl.endRefreshing()
            }
        }
    }
}

// MARK: - UICollectionViewDelegate

extension MoviesListViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let controller = UIViewController.viewController(type: MovieDetailsViewController.self) as? MovieDetailsViewController {
            controller.movieModel = dataStorage[safe: indexPath.row]
            navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == dataSource.items.count - 2 {
            loadNext()
        }
    }
}
