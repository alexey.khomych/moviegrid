//
//  MovieViewModel.swift
//  MovieGrid
//
//  Created by Alexey Khomych on 21.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import Foundation

class MoviesCollectionItemViewModel {
    
    // MARK: - Public Variable
    
    let posterURL: String
    
    // MARK: - Initializations
    
    init(posterURL: String) {
        self.posterURL = EnvironmentProviderImpl().loadEnviroment().basePosterUrl + posterURL
    }
}
