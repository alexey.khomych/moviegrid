//
//  MoviewCollectionViewLayout.swift
//  MovieGrid
//
//  Created by Alexey Khomych on 21.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import UIKit

class MoviesCollectionViewLayout: UICollectionViewFlowLayout {
    
    var cellPerRow: CGFloat = 2.0
    
    // MARK: - Layout
    
    override func prepare() {
        super.prepare()
        
        let minItemSpacing = Constants.minItemSpacing
        minimumInteritemSpacing = minItemSpacing
        minimumLineSpacing = 5
        
        sectionInset = UIEdgeInsets(top: minItemSpacing,
                                    left: Constants.leftInset,
                                    bottom: minItemSpacing,
                                    right: Constants.rightInset)
    
        itemSize = calculateItemSize()
    }
    
    // MARK: - Private
    
    private func calculateItemSize() -> CGSize {
        guard let collectionView = collectionView else { return .zero }
        let itemWidth = collectionView.bounds.width / cellPerRow - Constants.insets * cellPerRow
        
        return CGSize(width: itemWidth, height: itemWidth / Constants.heightToWidthRatio)
    }
}

// MARK: - Private

private extension MoviesCollectionViewLayout {
    
    enum Constants {
        static let minItemSpacing: CGFloat = 5
        static let insets: CGFloat = 10.0
        static let heightToWidthRatio: CGFloat = 0.7
        static let leftInset: CGFloat = 16.0
        static let rightInset: CGFloat = 16.0
    }
}
