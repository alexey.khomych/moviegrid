//
//  MoviesListCollectionViewCell.swift
//  MovieGrid
//
//  Created by Alexey Khomych on 21.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import UIKit
import Kingfisher

class MoviesListCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet private weak var posterImageView: UIImageView!
    
}

// MARK: - ModelTransfer

extension MoviesListCollectionViewCell: ModelTransfer {
    
    func update(with model: MoviesCollectionItemViewModel) {
        posterImageView.kf.indicatorType = .activity
        posterImageView.kf.setImage(with: URL(string: model.posterURL))
    }
}
