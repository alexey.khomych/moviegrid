//
//  MovieDetailsViewModelType.swift
//  MovieGrid
//
//  Created by Alexey Khomych on 22.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

enum MovieDetailsViewModelType {
    case details(MovieDetailsViewModel)
    case description(MovieDetailsDescriptionViewModel)
}
