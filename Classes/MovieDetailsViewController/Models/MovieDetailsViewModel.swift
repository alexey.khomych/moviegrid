//
//  MovieDetailsViewModel.swift
//  MovieGrid
//
//  Created by Alexey Khomych on 22.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import Foundation

class MovieDetailsViewModel {
    
    // MARK: - Public Variables
    
    let posterURL: String
    let score: String
    let rating: String
    let releaseDate: String
    let title: String
    
    // MARK: - Initializations
    
    init(posterURL: String, score: String, rating: String, releaseDate: String, title: String) {
        self.title = String(format: "%@ (%@)", title, releaseDate.dateFormat(desiredFormat: Constants.yearFormat))
        self.releaseDate = releaseDate.dateFormat(desiredFormat: Constants.preferredDateFormat)
        self.posterURL = EnvironmentProviderImpl().loadEnviroment().basePosterUrl + posterURL
        self.score = score
        self.rating = rating
    }
}

// MARK: - Private

private extension MovieDetailsViewModel {
    
    enum Constants {
        static let yearFormat = "yyyy"
        static let preferredDateFormat = "MMM d, yyyy"
    }
}
