//
//  MovieDetailsDescriptionViewModel.swift
//  MovieGrid
//
//  Created by Alexey Khomych on 22.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import Foundation

class MovieDetailsDescriptionViewModel {
    // MARK: - Public Variables
    
    let description: String
    
    // MARK: - Initializations
    
    init(description: String) {
        self.description = description
    }
}
