//
//  MovieDetailsDescriptionTableViewCell.swift
//  MovieGrid
//
//  Created by Alexey Khomych on 22.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import UIKit

class MovieDetailsDescriptionTableViewCell: UITableViewCell {
    
    // MARK: - IBOutlets
    
    @IBOutlet private weak var descriptionLabel: UILabel!
}

// MARK: - ModelTransfer

extension MovieDetailsDescriptionTableViewCell: ModelTransfer {
    
    func update(with model: MovieDetailsDescriptionViewModel) {
        descriptionLabel.text = model.description
    }
}
