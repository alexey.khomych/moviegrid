//
//  MovieDetailsInfoTableViewCell.swift
//  MovieGrid
//
//  Created by Alexey Khomych on 22.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import UIKit

class MovieDetailsInfoTableViewCell: UITableViewCell {
    
    // MARK: - IBOutlets
    
    @IBOutlet private weak var posterImageView: UIImageView!
    @IBOutlet private weak var scoreCountLabel: UILabel!
    @IBOutlet private weak var ratingTypeLabel: UILabel!
    @IBOutlet private weak var releaseDateLabel: UILabel!
    @IBOutlet private weak var titleLabel: UILabel!
}

// MARK: - ModelTransfer

extension MovieDetailsInfoTableViewCell: ModelTransfer {
    
    func update(with model: MovieDetailsViewModel) {
        posterImageView.kf.setImage(with: URL(string: model.posterURL))
        scoreCountLabel.text = model.score
        ratingTypeLabel.text = model.rating
        releaseDateLabel.text = model.releaseDate
        titleLabel.text = model.title
    }
}
