//
//  MovieDetailsCellFactory.swift
//  MovieGrid
//
//  Created by Alexey Khomych on 22.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import UIKit

protocol MovieDetailsCellFactory: MultipleCellsFactory { }

class MovieDetailsCellFactoryImpl: MovieDetailsCellFactory {
    
    // MARK: - MultipleCellsFactory
    
    func cellForModel(_ model: MovieDetailsViewModelType, tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        switch model {
        case let .details(details):
            let cell: MovieDetailsInfoTableViewCell = tableView.dequeueReusableCell(for: indexPath)
            cell.update(with: details)
            cell.backgroundColor = .clear
            return cell
        case let .description(description):
            let cell: MovieDetailsDescriptionTableViewCell = tableView.dequeueReusableCell(for: indexPath)
            cell.update(with: description)
            cell.backgroundColor = .clear
            return cell
        }
    }
}
