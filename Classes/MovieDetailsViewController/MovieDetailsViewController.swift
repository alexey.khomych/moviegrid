//
//  MovieDetailsViewController.swift
//  MovieGrid
//
//  Created by Alexey Khomych on 21.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import UIKit

class MovieDetailsViewController: UIViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet private weak var tableView: UITableView!
    
    // MARK: - Public Variable
    
    var movieModel: MovieModel!
    
    // MARK: - Private Variable
    
    private var dataSource = MultipleCellsArrayTableViewDataSource<MovieDetailsViewModelType, MovieDetailsCellFactoryImpl>()
    private let builder = MovieDetailsDataSourceBuilderImpl()

    override func viewDidLoad() {
        super.viewDidLoad()

        configureUI()
        buildDataSource(movieModel)
    }
}

// MARK: - Private

private extension MovieDetailsViewController {
    
    enum Constants {
        static let backTitle = "back"
    }
    
    func configureUI() {
        tableView.backgroundColor = .clear
        tableView.dataSource = dataSource
    
        configureBlurBackground()
        configureNavigationBarItems()
    }
    
    func configureNavigationBarItems() {
        navigationItem.leftBarButtonItem?.title = Constants.backTitle
        navigationItem.title = movieModel.title
    }
    
    func configureBlurBackground() {
        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        view.addSubview(blurEffectView)
        view.sendSubviewToBack(blurEffectView)
    }
    
    func buildDataSource(_ model: MovieModel) {
        dataSource.cellsFactory = MovieDetailsCellFactoryImpl()
        dataSource.append(items: builder.build(with: model))
        tableView.hideEmptySeparators()
        tableView.reloadData()
    }
}
