//
//  MovieDetailsDataSourceBuilder.swift
//  MovieGrid
//
//  Created by Alexey Khomych on 22.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import Foundation

protocol MovieDetailsDataSourceBuilder {
    func build(with model: MovieModel) -> [MovieDetailsViewModelType]
}

struct MovieDetailsDataSourceBuilderImpl: MovieDetailsDataSourceBuilder {
    
    func build(with model: MovieModel) -> [MovieDetailsViewModelType] {
        let detailsViewModel = MovieDetailsViewModel(posterURL: model.posterPath ?? "",
                                                     score: "\(model.voteAverage)",
                                                     rating: "\(model.popularity)",
                                                     releaseDate: model.releaseDate,
                                                     title: model.title)
        
        let descriptionViewModel = MovieDetailsDescriptionViewModel(description: model.overview)
        
        return [.details(detailsViewModel), .description(descriptionViewModel)]
    }
}
