//
//  BaseRouterProtocol.swift
//  MovieGrid
//
//  Created by Alexey Khomych on 21.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import Alamofire

protocol BaseRouterProtocol: URLRequestConvertible {
    var additionalHeaders: [String: String]? { get }
    var httpMethod: Alamofire.HTTPMethod { get }
    var path: String { get }
    var queryParameters: [String: String]? { get }
    var baseURL: String { get }
}

extension BaseRouterProtocol {
    
    var baseURL: String {
        return EnvironmentProviderImpl().loadEnviroment().baseUrl
    }
    
    var enviroment: EnvironmentType {
        return EnvironmentProviderImpl().loadEnviroment()
    }

    func asURLRequest() throws -> URLRequest {
        let apiKey = enviroment.apiKey
        
        guard let url = URL(string: baseURL + path + "?api_key=\(apiKey)") else {
            fatalError("No path components in BaseRouterProtocol")
        }
        var urlRequest = URLRequest(url: url)
        urlRequest.allHTTPHeaderFields = ["Content-Type": "application/json"]
        urlRequest.httpMethod = httpMethod.rawValue
        additionalHeaders?.forEach { urlRequest.setValue($0.value, forHTTPHeaderField: $0.key) }

        if let parameters = queryParameters {
            urlRequest = try URLEncoding.queryString.encode(urlRequest, with: parameters)
        }
        
        return urlRequest
    }
}

extension BaseRouterProtocol {
    var additionalHeaders: [String: String]? { return nil }
    var queryParameters: [String: String]? { return nil }
}
