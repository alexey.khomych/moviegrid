//
//  MovieRouter.swift
//  MovieGrid
//
//  Created by Alexey Khomych on 21.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import Alamofire

enum MovieRouter {
    case nowPlaying(page: Int)
}

// MARK: - BaseRouterProtocol

extension MovieRouter: BaseRouterProtocol {
    
    var httpMethod: HTTPMethod {
        return .get
    }
    
    var path: String {
        switch self {
        case .nowPlaying:
            return "movie/now_playing"
        }
    }
    
    var queryParameters: [String: String]? {
        switch self {
        case let .nowPlaying(page):
            var parameters: [String: String] = [:]
            parameters["page"] = "\(page)"
            return parameters
        }
    }
}
