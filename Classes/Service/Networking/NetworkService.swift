//
//  NetworkService.swift
//  MovieGrid
//
//  Created by Alexey Khomych on 21.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import Foundation
import Alamofire

typealias NetworkRequestCompletion<T: Decodable, E: Error> = (Swift.Result<T, APIError>) -> Void

protocol NetworkService {
    func nowPlayingList(_ page: Int, completion: @escaping NetworkRequestCompletion<ResultModel, APIError>)
}

class NetworkServiceImpl {
    
    func nowPlayingList(_ page: Int, completion: @escaping NetworkRequestCompletion<ResultModel, APIError>) {
        var storedError: APIError?
        let router = MovieRouter.nowPlaying(page: page)

        _ = Alamofire.request(router).responseJSON { (dataResponse) in
            switch dataResponse.result {
            case .success:
                if let data = dataResponse.data {
                    do {
                        let response = try JSONDecoder().decode(ResultModel.self, from: data)
                        completion(.success(response))
                    } catch {
                        print(error.localizedDescription)
                        storedError = APIError(errorDescription: error.localizedDescription)
                    }
                }
            case let .failure(error):
                storedError = APIError(errorDescription: error.localizedDescription)
            }

            if let storedError = storedError {
                completion(.failure(storedError))
            }
        }
    }
}
