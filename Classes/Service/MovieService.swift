//
//  MovieService.swift
//  MovieGrid
//
//  Created by Alexey Khomych on 21.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import Foundation

typealias MovieListHandler = (Swift.Result<[MovieModel], APIError>) -> ()

protocol MovieService {
    func nowPlaying(handler: @escaping MovieListHandler)
    func loadNext(handler: @escaping MovieListHandler)
}

class MovieServiceImpl {
    
    // MARK: - Private Variable
    
    private let networkService = NetworkServiceImpl()
    private var currentPage = 1
    private var pagesCount: Int?
}

// MARK: - Private

private extension MovieServiceImpl {
    
    enum Constants {
        static let defaultPage = 1
    }
    
    func loadData(_ page: Int, handler: @escaping MovieListHandler) {
        networkService.nowPlayingList(page) { (result) in
            switch result {
            case let .success(list):
                handler(.success(list.results))
                self.currentPage += 1
            case let .failure(error):
                handler(.failure(error))
            }
        }
    }
}

// MARK: - MovieService

extension MovieServiceImpl: MovieService {
    
    func nowPlaying(handler: @escaping MovieListHandler) {
        loadData(Constants.defaultPage, handler: handler)
    }
    
    func loadNext(handler: @escaping MovieListHandler) {
        loadData(currentPage, handler: handler)
    }
}
