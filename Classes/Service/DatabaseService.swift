//
//  DatabaseService.swift
//  MovieGrid
//
//  Created by Alexey Khomych on 21.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import CoreData

protocol DatabaseService {
    var persistentContainer: NSPersistentContainer! { get }
    
    func saveContext()
}

class DatabaseServiceImpl: DatabaseService {
    
    static let instance = DatabaseServiceImpl()
    
    var persistentContainer: NSPersistentContainer! {
        let container = NSPersistentContainer(name: "MovieGrid")
        container.loadPersistentStores(completionHandler: { (_, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }
    
    func saveContext() {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}
