//
//  Collection+Convenience.swift
//  MovieGrid
//
//  Created by Alexey Khomych on 23.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import Foundation

extension Collection {

    /// Returns the element at the specified index if it is within bounds, otherwise nil.
    subscript(safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
