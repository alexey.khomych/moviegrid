//
//  String+DateFormatter.swift
//  MovieGrid
//
//  Created by Alexey Khomych on 22.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import Foundation

extension String {
    
    func dateFormat(_ sourceFormat: String = "yyyy-mm-dd", desiredFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = sourceFormat
        
        var formattedDate = ""
        
        if let date = dateFormatter.date(from: self) {
            dateFormatter.dateFormat = desiredFormat
            formattedDate = dateFormatter.string(from: date)
        }
        
        return formattedDate
    }
}
