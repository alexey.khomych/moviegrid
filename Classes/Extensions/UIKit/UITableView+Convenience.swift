//
//  UITableView+Convenience.swift
//  MovieGrid
//
//  Created by Alexey Khomych on 22.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
    
    func hideEmptySeparators() {
       tableFooterView = UIView(frame: CGRect.zero)
     }
}
