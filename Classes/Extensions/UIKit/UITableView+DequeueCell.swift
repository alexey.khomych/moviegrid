//
//  UITableView+DequeueCell.swift
//  MovieGrid
//
//  Created by Alexey Khomych on 22.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import UIKit

extension UITableView {
    
    /**
     The shorter method for reusable cell registering
     */
    func dequeueReusableCell<T: UITableViewCell>(for indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withIdentifier: T.reuseIdentifier(), for: indexPath) as? T else {
            #if DEBUG
            fatalError("Could not dequeue cell with identifier: \(T.reuseIdentifier())")
            #else
            return T()
            #endif
        }
        
        return cell
    }
}
