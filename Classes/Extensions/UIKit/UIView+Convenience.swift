//
//  UIView+Convenience.swift
//  MovieGrid
//
//  Created by Alexey Khomych on 21.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import UIKit

extension UIView {
    static var nib: UINib {
        let bundle = Bundle(for: self as AnyClass)
        let nib = UINib(nibName: String(describing: self), bundle: bundle)
        return nib
    }

    static func loadFromNib() -> Self {
        return _loadFromNib()
    }

    // MARK: - Private

    fileprivate static func _loadFromNib<T: UIView>() -> T {
        guard let view = nib.instantiate(withOwner: nil, options: nil).first as? T else {
            fatalError("The nib \(nib) expected its root view to be of type \(self)")
        }
        return view
    }
}
