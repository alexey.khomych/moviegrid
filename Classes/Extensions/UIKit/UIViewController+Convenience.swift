//
//  UIViewController+Convenience.swift
//  MovieGrid
//
//  Created by Alexey Khomych on 23.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import UIKit

extension UIViewController {
    static func viewController(type: UIViewController.Type) -> UIViewController {
        let storyboardId = String(describing: type)
        let storyBoard = UIStoryboard(name: storyboardId, bundle: nil)
        return storyBoard.instantiateViewController(withIdentifier: storyboardId)
    }
}
